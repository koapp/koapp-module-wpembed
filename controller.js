angular
  .controller('wpembedCtrl', loadFunction);

loadFunction.$inject = ['$scope', 'structureService', 'storageService', '$location', '$translate', '$timeout', '$rootScope'];

function loadFunction($scope, structureService, storageService, $location, $translate, $timeout, $rootScope) {
  //Register upper level modules
  structureService.registerModule($location, $scope, 'wpembed',  $translate.use());

    var lang = $translate.use().replace('_', '-');
    var config = $scope.wpembed.modulescope;
    var configLang = $scope.wpembed.modulescopeLang && $scope.wpembed.modulescopeLang[lang] && $scope.wpembed.modulescopeLang[lang].langUrl != "" ? $scope.wpembed.modulescopeLang[lang]: false;
    
    
    $scope.loader = {
        enabled :   config.enabledLoader,
        img     :   config.img,
        text    :   config.text
    }
    $scope.url = config.url;
    
    $scope.loading = true;
   $timeout(function() { $scope.loading = false; }, 1000 * config.delay);
    
    if (configLang) {
      $scope.url = configLang.langUrl;
      if(configLang.img != "" && configLang.text != ""){
       $scope.loader = {
             enabled :   config.enabledLoader,
            img     :   configLang.langimg,
            text:    configLang.langtext
       }
      }
    }
    structureService.launchSpinner('.transitionloader');
    
    window.addEventListener("message", function(event) {
      if(event.data.toDownload && event.data.toDownload != ""){
          downloadFile(event.data.toDownload);
      }
      if(event.data.toOpen){
          cordova.InAppBrowser.open(event.data.toOpen, '_system', 'location=yes');
      }
      
    });
    
    function downloadFile( fileUrl ){
        if(!precheck(fileUrl)) {throwError("Invalid File Url"); return};
      
         //show message while downloading
         statusBarManager("Downloading");
        
        let file = fileUrl.match(/[^\/]*$/)[0].split("#")[0].split("?")[0];
        
        //check if service embed downloads has created the download interface
        if($rootScope.kingofapp && $rootScope.kingofapp.actions && $rootScope.kingofapp.actions.downloadFile){
            $rootScope.kingofapp.actions.downloadFile(event.data.toDownload, "Download/" + file , (status)=>{
                console.log(status);
                statusBarManager(status.msg, "message", 2000);
                
            });
        }
    }
    
    function precheck(url){
        let file = url.match(/[^\/]*$/)[0].split("#")[0].split("?")[0];

        let fileExtension = isFile(file)? file.split(".")[1] : null;
        
        return validateUrl(url) && isValidHttpUrl(url) && validExtension(fileExtension);
    }


    //tools functions
    function throwError(error){
        console.error(error);
        statusBarManager("Error "+ error, "error", 2000 );
    }
    
    
    function statusBarManager(message, type="message", disableTime){
       
        $scope.statusBarClass = type; //error warning message
        $scope.message = message;
        
        if(disableTime){
            setTimeout(()=>{
                $scope.showMsgBar = false;
            }, disableTime);
        }
        
        $scope.showMsgBar = true;
    }
    
    function validateUrl(value) {
          return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
        }
    
    
    function isFile(fileName){
    	return fileName.indexOf(".") > 0 && fileName.indexOf(".") < fileName.length;
    }
    
    function isValidHttpUrl(string) {
      let url;
      
      try {
        url = new URL(string);
      } catch (_) {
        return false;  
      }
    
      return url.protocol === "http:" || url.protocol === "https:";
    }
    
    function validExtension(extension){
    	let allowed = [
    		"pdf",
    		"img",
    		"txt"
    	];
    	return extension != null && allowed.indexOf(extension) != -1;
    }
    
}
