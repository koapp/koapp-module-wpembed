
# **(How) Configure the insert module**



Although its use is not especially recommended (to the markets and especially **Apple**, they do not like this format) there are times when there is no choice but to **embed a web page in an application**. For these cases, in King of App we have developed this module of quick and easy configuration.



As it is embedded content, just fill in the field enabled with **the destination URL**. Nothing more and nothing less. We take care that it appears on the screen taking the **receptive parameters** of the web.



Sometimes you will want to keep a mobile web version of your website as well as include some of its content in an application. For these scenarios we have developed a wordpress plugin that allows you to have a third version of your website with custom css. In this way it is possible to have your mobile website in addition to a version of the application with additional content.

To use it you just have to install the plugin:

https://s3.eu-central-1.amazonaws.com/kingofapp.com/wp-koa-embed.zip

Once activated you can configure it from the menu **Settings / Koa Embed**

To use it add **[[Param. Query]] = true** at the end of the url in your module configuration. *Don't forget to replace [[Query parameter]] with the value you have set in your WordPress*



That said, although this module can help multiple times, like all great power, you have to know how to use it in moderation and always combined with other content modules.



**In case of problems with the scroll on ios:**



Add the following line in the css of your website:



`` @media screen and (min-width: 250px) and (max-width: 1024px) {html, body {height: 100%! important; overflow-y: scroll! important; -webkit-overflow-scrolling: touch! important;}} ``